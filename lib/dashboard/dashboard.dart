import 'package:fluttertest/error_internet.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:fluttertest/layout.dart';
import 'package:fluttertest/loading.dart';
import 'package:fluttertest/sharedpref_service.dart';
import 'package:fluttertest/model/user_response.dart';
import 'package:fluttertest/model/login_response.dart';
import 'package:fluttertest/api_service.dart';
import 'package:flutter/services.dart';
import 'package:fluttertest/signin.dart';
import 'package:fluttertest/signup.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border.all(
      width: 3.0,
      color: Colors.blue[100],
    ),
    borderRadius: BorderRadius.all(
        Radius.circular(10.0) //                 <--- border radius here
        ),
    color: Colors.white,
  );
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    SizeConfig().init(context);
    return WillPopScope(
        onWillPop: () {
          return showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text('Keluar dari aplikasi'),
                      content:
                          Text('Apakah anda yakin ingin keluar aplikasi ?'),
                      actions: <Widget>[
                        FlatButton(
                          child: Text('batal'),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        FlatButton(
                          child: Text('keluar'),
                          onPressed: () {
                            SystemChannels.platform
                                .invokeMethod('SystemNavigator.pop');
                          },
                        )
                      ],
                    );
                  }) ??
              false;
        },
        child: Scaffold(
            resizeToAvoidBottomPadding: false,
            body: Stack(children: <Widget>[
              Center(
                  child: Column(children: <Widget>[
                Container(
                    width: SizeConfig.blockHorizontal * 90,
                    height: SizeConfig.blockVertical * 60,
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(
                        top: SizeConfig.blockVertical * 40,
                        bottom: SizeConfig.blockVertical * 1),
                    child: Image.asset(
                        "assets/img/login_or_signup/kertaskomputer.png")),
                Container(
                  width: SizeConfig.blockHorizontal * 90,
                  child: Container(
                    child: Text(
                      "Welcome to the app",
                      textAlign: TextAlign.center,
                      style: new TextStyle(
                        fontSize: SizeConfig.blockHorizontal * 6,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: SizeConfig.blockVertical * 5.5),
                  width: SizeConfig.blockHorizontal * 90,
                  child: Container(
                    child: Text(
                      "Lorem ipsum dolor sit amet, sit exerci fierent repudiandae eu. At impedit persequeris accommodare sea, vel dicit consul officiis in, no cum duis delicata. Mel errem blandit mediocritatem ad, mei percipit interesset ut, ut vel lorem suscipiantur. Mel te utamur petentium. Eos quot oblique facilis te. Delectus phaedrum id vis. Velit scaevola corrumpit sit id, ei sea porro error choro. Ne eum ludus accommodare, duo cu tempor eleifend. Sit utroque alienum te. Eius lorem ex eam, eam in quaeque voluptaria definiebas. Sed quod omnes no, at eum nostrum intellegebat.",
                      textAlign: TextAlign.center,
                      style: new TextStyle(
                        fontSize: SizeConfig.blockHorizontal * 3,
                      ),
                    ),
                  ),
                ),
              ]))
            ])));
  }
}
