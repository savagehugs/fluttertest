import 'dart:convert';

class UserResponse {
  int id;
  String email;
  String tipe;
  String nama;
  String no_hp;
  String alamat;
  String foto;
  int id_pesan;

  UserResponse({this.id, this.email, this.tipe, this.nama, this.no_hp, this.alamat, this.foto, this.id_pesan});

  factory UserResponse.fromJson(Map<String, dynamic> map) {
    return UserResponse(
        id: map["id"], email: map["email"], tipe: map["tipe"], nama: map["nama"], no_hp: map["no_hp"], alamat: map["alamat"], foto: map["foto"], id_pesan: map["id_pesan"]);
  }

  Map<String, dynamic> toJson() {
    return {"id": id, "email": email, "tipe": tipe, "nama": nama, "no_hp": no_hp, "alamat": alamat, "foto": foto, "id_pesan": id_pesan};
  }

  @override
  String toString() {
    return 'UserResponse{id: $id, email: $email, tipe: $tipe, nama: $nama, no_hp: $no_hp, alamat: $alamat, foto: $foto, id_pesan: $id_pesan}';
  }

}

UserResponse userResponseFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return UserResponse.fromJson(data);
}

String userResponseToJson(UserResponse data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}