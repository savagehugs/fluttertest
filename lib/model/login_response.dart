import 'dart:convert';

class LoginResponse {
  String access_token;
  String token_type;
  String expires_at;

  LoginResponse({this.access_token, this.token_type, this.expires_at});

  factory LoginResponse.fromJson(Map<String, dynamic> map) {
    return LoginResponse(
        access_token: map["access_token"], token_type: map["token_type"], expires_at: map["expires_at"]);
  }

  Map<String, dynamic> toJson() {
    return {"access_token": access_token, "token_type": token_type, "expires_at": expires_at};
  }

  @override
  String toString() {
    return 'LoginResponse{access_token: $access_token, token_type: $token_type, expires_at: $expires_at}';
  }

}

LoginResponse loginResponseFromJson(String jsonData) {
  final data = json.decode(jsonData);
  return LoginResponse.fromJson(data);
}

String loginResponseToJson(LoginResponse data) {
  final jsonData = data.toJson();
  return json.encode(jsonData);
}