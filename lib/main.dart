import 'package:flutter/material.dart';
import 'package:fluttertest/signin.dart';
import 'package:fluttertest/firstscreen.dart';
import 'package:fluttertest/dashboard/dashboard.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: new Firstscreen(),
      theme: ThemeData(
          // Use the old theme but apply the following three changes
          textTheme: Theme.of(context)
              .textTheme
              .apply(fontFamily: 'Roboto', displayColor: Colors.black)),
      routes: <String, WidgetBuilder>{
        '/Signin': (BuildContext context) => new Signin(),
        '/Dashboard': (BuildContext context) => new Dashboard(),
      },
    );
  }
}
