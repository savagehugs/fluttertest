import 'package:fluttertest/error_internet.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:fluttertest/layout.dart';
import 'package:fluttertest/loading.dart';
import 'package:fluttertest/sharedpref_service.dart';
import 'package:fluttertest/model/user_response.dart';
import 'package:fluttertest/model/login_response.dart';
import 'package:fluttertest/api_service.dart';
import 'package:flutter/services.dart';

class Signup extends StatefulWidget {
  @override
  _SignupState createState() => _SignupState();
}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    border: Border.all(
      width: 3.0,
      color: Colors.blue[100],
    ),
    borderRadius: BorderRadius.all(
        Radius.circular(10.0) //                 <--- border radius here
        ),
    color: Colors.white,
  );
}

class _SignupState extends State<Signup> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final apiService = ApiService();
  bool loading = false;
  final _formKey = GlobalKey<FormState>();
  bool isValidAuth = true;
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  void doLogin(context) {
    if (_formKey.currentState.validate()) {
      final body = {
        "email": emailController.text,
        "password": passwordController.text
      };
      setState(() {
        loading = true;
      });
      apiService.doLogin(body).then((success) {
        if (success is LoginResponse) {
          apiService.getUser(token: success.access_token).then((success1) {
            if (success1 is UserResponse) {
              SharedPref.save("userResponse", userResponseToJson(success1));
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/Dashboard', (Route<dynamic> route) => false);
            } else {
              print(success1);
            }
          });
        } else {
          setState(() {
            isValidAuth = false;
            _formKey.currentState.validate();
            isValidAuth = true;
            loading = false;
          });
        }
      }, onError: (e) {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ErrorInternet()),
        );
      });
    }
  }

  Widget showLoading() {
    if (loading)
      return Container(
        alignment: Alignment.center,
        color: Color.fromRGBO(255, 255, 255, 0.5),
        child: Loading(
          color1: Colors.redAccent,
          color2: Colors.deepPurple,
          color3: Colors.green,
        ),
      );
    else
      return Container();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    SizeConfig().init(context);
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Center(
          child: new Image.asset(
            'assets/img/login_or_signup/signin.PNG',
            width: size.width,
            height: size.height,
            fit: BoxFit.fill,
          ),
        ),
        ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                GestureDetector(
                  child: Container(
                    padding: EdgeInsets.only(
                        left: SizeConfig.blockHorizontal * 5,
                        top: SizeConfig.blockHorizontal * 5),
                    alignment: Alignment.topLeft,
                    child: new Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                      size: SizeConfig.blockHorizontal * 6,
                    ),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                Container(
                  padding: const EdgeInsets.all(20.0),
                  margin: EdgeInsets.only(
                    top: SizeConfig.blockHorizontal * 90,
                  ),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(color: Colors.white, offset: Offset(0, 0))
                      ],
                      borderRadius: new BorderRadius.only(
                          topRight: Radius.circular(50),
                          topLeft: Radius.circular(50))),
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25, right: 25),
                    child: Column(
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(
                                  top: SizeConfig.blockVertical * 5),
                              width: SizeConfig.blockHorizontal * 90,
                              child: Container(
                                child: Text(
                                  "Register new account",
                                  textAlign: TextAlign.left,
                                  style: new TextStyle(
                                    fontSize: SizeConfig.blockHorizontal * 5,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 0),
                            child: Form(
                              key: _formKey,
                              autovalidate: false,
                              child: Column(
                                children: <Widget>[
                                  SizedBox(
                                    height: SizeConfig.blockVertical * 5,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 0),
                                    child: TextFormField(
                                      keyboardType: TextInputType.emailAddress,
                                      autofocus: false,
                                      controller: emailController,
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return 'Isikan email';
                                        }
                                        if (!isValidAuth) {
                                          return 'Email atau Password tidak sesuai ';
                                        }
                                        Pattern pattern =
                                            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                        RegExp regex = new RegExp(pattern);
                                        if (!regex.hasMatch(value))
                                          return 'Masukan email yg valid';

                                        return null;
                                      },
                                      style: TextStyle(
                                          fontSize:
                                              SizeConfig.blockHorizontal * 4.5),
                                      decoration: InputDecoration(
                                        filled: true,
                                        fillColor: Colors.white,
                                        hintText: 'Email',
                                        contentPadding: EdgeInsets.fromLTRB(
                                            0.0, 10.0, 20.0, 10.0),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: SizeConfig.blockVertical * 5,
                                  ),
                                  TextFormField(
                                    autofocus: false,
                                    obscureText: true,
                                    controller: passwordController,
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return 'Isikan password';
                                      }
                                      return null;
                                    },
                                    style: TextStyle(
                                        fontSize:
                                            SizeConfig.blockHorizontal * 4.5),
                                    decoration: InputDecoration(
                                      filled: true,
                                      fillColor: Colors.white,
                                      hintText: 'Password',
                                      contentPadding: EdgeInsets.fromLTRB(
                                          0.0, 10.0, 20.0, 10.0),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          child: Container(
                            height: SizeConfig.blockVertical * 10,
                            margin: EdgeInsets.only(
                                top: SizeConfig.blockHorizontal * 15),
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(53, 73, 251, 1),
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.white, offset: Offset(0, 0))
                                ],
                                borderRadius: new BorderRadius.circular(50.0)),
                            child: Center(
                              child: Text(
                                "Create Account",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: SizeConfig.blockHorizontal * 4.8),
                              ),
                            ),
                          ),
                          onTap: () {
                            doLogin(context);
                          },
                        ),
                        SizedBox(
                          height: SizeConfig.blockVertical * 30,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        showLoading()
      ],
    ));
  }
}
