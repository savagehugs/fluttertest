import 'package:fluttertest/api_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ErrorInternet extends StatefulWidget {
  @override
  _ErrorInternetState createState() => _ErrorInternetState();
}

class _ErrorInternetState extends State<ErrorInternet> {
  bool isTryAgain = false;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        return showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text('Keluar dari aplikasi'),
                    content: Text('Apakah anda yakin ingin keluar aplikasi ?'),
                    actions: <Widget>[
                      FlatButton(
                        child: Text('Batal'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                      FlatButton(
                        child: Text('Keluar'),
                        onPressed: () {
                          SystemChannels.platform
                              .invokeMethod('SystemNavigator.pop');
                        },
                      )
                    ],
                  );
                }) ??
            false;
      },
      child: Scaffold(
        body: Container(
          alignment: Alignment.center,
          child: isTryAgain
              ? CircularProgressIndicator()
              : InkWell(
                  child: Padding(
                    padding: const EdgeInsets.all(32.0),
                    child: Text('Internet Error'),
                  ),
                  onTap: () {
                    setState(() {
                      isTryAgain = true;
                    });
                    ApiService apiService = ApiService();
                    apiService.testInternet().then((sukses) {
                      Navigator.of(context).pop();
                    }, onError: (e) {
                      setState(() {
                        isTryAgain = false;
                      });
                    }).catchError((e) {
                      setState(() {
                        isTryAgain = false;
                      });
                    });
                  }),
        ),
      ),
    );
  }
}
