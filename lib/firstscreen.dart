import 'package:fluttertest/error_internet.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:fluttertest/layout.dart';
import 'package:fluttertest/loading.dart';
import 'package:fluttertest/sharedpref_service.dart';
import 'package:fluttertest/model/user_response.dart';
import 'package:fluttertest/model/login_response.dart';
import 'package:fluttertest/api_service.dart';
import 'package:flutter/services.dart';
import 'package:fluttertest/signin.dart';
import 'package:fluttertest/signup.dart';

class Firstscreen extends StatefulWidget {
  Firstscreen() {
    SystemChrome.setEnabledSystemUIOverlays([]);
  }
  @override
  _FirstscreenState createState() => _FirstscreenState();
}

BoxDecoration myBoxDecoration() {
  return BoxDecoration(
    gradient: LinearGradient(
      begin: Alignment.topCenter,
      end: Alignment.bottomCenter, // 10% of the width, so there are ten blinds.
      colors: [
        const Color(0xFF3549FB),
        const Color(0xFF4ED2DA)
      ], // red to yellow
      tileMode: TileMode.repeated, // repeats the gradient over the canvas
    ),
  );
}

class _FirstscreenState extends State<Firstscreen> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    SizeConfig().init(context);
    return WillPopScope(
        onWillPop: () {
          return showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return AlertDialog(
                      title: Text('Keluar dari aplikasi'),
                      content:
                          Text('Apakah anda yakin ingin keluar aplikasi ?'),
                      actions: <Widget>[
                        FlatButton(
                          child: Text('batal'),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        ),
                        FlatButton(
                          child: Text('keluar'),
                          onPressed: () {
                            SystemChannels.platform
                                .invokeMethod('SystemNavigator.pop');
                          },
                        )
                      ],
                    );
                  }) ??
              false;
        },
        child: Scaffold(
            resizeToAvoidBottomPadding: false,
            body: Stack(children: <Widget>[
              Center(
                child: new Image.asset(
                  'assets/img/login_or_signup/signinorlogin.PNG',
                  width: size.width,
                  height: size.height,
                  fit: BoxFit.fill,
                ),
              ),
              Container(
                  margin:
                      EdgeInsets.only(top: SizeConfig.blockHorizontal * 120),
                  child: Column(
                    children: <Widget>[
                      GestureDetector(
                        child: Container(
                          width: SizeConfig.blockHorizontal * 60,
                          height: SizeConfig.blockVertical * 10,
                          margin: EdgeInsets.only(
                              left: SizeConfig.blockHorizontal * 20,
                              top: SizeConfig.blockHorizontal * 10),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.white, offset: Offset(0, 0))
                              ],
                              borderRadius: new BorderRadius.circular(50.0)),
                          child: Center(
                            child: Text(
                              "Register Account",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: SizeConfig.blockHorizontal * 4.8),
                            ),
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Signup()),
                          );
                        },
                      ),
                      GestureDetector(
                        child: Container(
                          width: SizeConfig.blockHorizontal * 60,
                          height: SizeConfig.blockVertical * 10,
                          margin: EdgeInsets.only(
                              left: SizeConfig.blockHorizontal * 20,
                              top: SizeConfig.blockHorizontal * 10),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.white, offset: Offset(0, 0))
                              ],
                              borderRadius: new BorderRadius.circular(50.0)),
                          child: Center(
                            child: Text(
                              "Login",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: SizeConfig.blockHorizontal * 4.8),
                            ),
                          ),
                        ),
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Signin()),
                          );
                        },
                      ),
                    ],
                  ))
            ])));
  }
}
