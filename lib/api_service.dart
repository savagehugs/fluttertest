import 'package:fluttertest/model/login_response.dart';
import 'package:fluttertest/model/user_response.dart';
import 'package:http/http.dart' show Client;
import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:fluttertest/sharedpref_service.dart';
import 'dart:convert';
import 'dart:io';
import 'package:http_parser/http_parser.dart';
import 'package:dio/dio.dart';

class ApiService {
  final String baseUrl = "https://art-language-sport-centre.com";
  Client client = Client();
  LoginResponse loginResponse;
  final Dio dio = new Dio();
  ApiService() {
    this.loadSharedPrefs();
  }
  loadSharedPrefs() async {
    try {
      loginResponse =
          loginResponseFromJson(await SharedPref.read("loginResponse"));
    } catch (Excepetion) {}
  }

  Future<UserResponse> getUser({String token = "token"}) async {
    if (token == 'token') {
      loadSharedPrefs();
      final response = await client.get("$baseUrl/api/ortu/user", headers: {
        HttpHeaders.authorizationHeader:
            loginResponse.token_type + " " + loginResponse.access_token
      });
      if (response.statusCode == 200) {
        return userResponseFromJson(response.body);
      } else {
        return null;
      }
    } else {
      final response = await client.get("$baseUrl/api/ortu/user",
          headers: {HttpHeaders.authorizationHeader: "Bearer" + " " + token});
      if (response.statusCode == 200) {
        return userResponseFromJson(response.body);
      } else {
        return null;
      }
    }
  }

  Future<LoginResponse> doLogin(body) async {
    final response = await client.post("$baseUrl/api/ortu/login", body: body);
    print(body);
    if (response.statusCode == 200) {
      SharedPref.save("loginResponse",
          loginResponseToJson(loginResponseFromJson(response.body)));
      return loginResponseFromJson(response.body);
    } else {
      return null;
    }
  }

  Future<String> testInternet() async {
    final response = await client.get("https://www.google.co.id");
    return "Sukses";
  }
}
